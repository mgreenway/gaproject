package org.epscor.uhhgatsp.mutator;

import org.epscor.uhhgatsp.citylist.ListXY;
import org.epscor.uhhgatsp.citylist.SimpleCityList;
import org.epscor.uhhgatsp.shift.PointShift;

/**
 * 
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public class CityListMutator implements Mutator
{
	@Override
	public ListXY Mutate(ListXY original, PointShift shift, double standardDev)
	{
		SimpleCityList mutatedCities = new SimpleCityList();

		int numCities = original.length();

		for (int i = 0; i < numCities; i++)
		{
			double oldX = original.getX(i);
			double oldY = original.getY(i);

			double newX = shift.newValue(oldX, standardDev);
			double newY = shift.newValue(oldY, standardDev);

			mutatedCities.set(i, newX, newY);
		}

		return mutatedCities;
	}

	@Override
	public ListXY Mutate(ListXY original, PointShift shift, double standardDev,
			double mutProb)
	{
		SimpleCityList mutatedCities = new SimpleCityList();

		int numCities = original.length();

		for (int i = 0; i < numCities; i++)
		{
			if (Math.random() <= mutProb)
			{
				double oldX = original.getX(i);
				double oldY = original.getY(i);

				double newX = shift.newValue(oldX, standardDev);
				double newY = shift.newValue(oldY, standardDev);

				mutatedCities.set(i, newX, newY);
			}
		}

		return mutatedCities;
	}

}
