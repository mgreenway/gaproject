package org.epscor.uhhgatsp.mutator;

import org.epscor.uhhgatsp.citylist.ListXY;
import org.epscor.uhhgatsp.shift.PointShift;

/**
 * 
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public interface Mutator
{
	ListXY Mutate(ListXY original, PointShift shift, double standardDev);

	ListXY Mutate(ListXY original, PointShift shift, double standardDev,
			double mutProb);
}
