package org.epscor.uhhgatsp.citylist;

/**
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 *
 */
public interface HasXY
{
	double getX();
	double getY();
}
