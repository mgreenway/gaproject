package org.epscor.uhhgatsp.citylist;

/**
 * 
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public interface ListXYMutable extends ListXY
{
	void set(int index, double x, double y);
}
