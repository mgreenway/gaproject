package org.epscor.uhhgatsp.citylist;

/**
 * 
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public interface ListXY
{
	double getX(int index) throws IndexOutOfBoundsException;

	double getY(int index) throws IndexOutOfBoundsException;

	int length();
}
