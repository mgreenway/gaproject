package org.epscor.uhhgatsp.citylist;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public class TSPPrinter
{
	private String name = "New City List";
	private String type = "TSP";
	private String comment;
	private String weight = "EUC_2D";
	private String start = "NODE_COORD_SECTION";
	private String end = "EOF";

	public List<String> print(ListXY list)
	{
		List<String> lines = new ArrayList<String>();

		lines.add("NAME: " + name);
		lines.add("TYPE: " + type);
		lines.add("COMMENT: " + comment);

		int size = list.length();

		lines.add("DIMENSION: " + size);
		lines.add("EDGE_WEIGHT_TYPE: " + weight);
		lines.add(start);

		for (int i = 0; i < size; ++i)
		{
			String line = (i + 1) + " ";
			line += list.getX(i) + " ";
			line += list.getY(i);

			lines.add(line);
		}

		lines.add(end);

		return lines;
	}

}
