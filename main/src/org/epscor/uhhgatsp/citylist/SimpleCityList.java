package org.epscor.uhhgatsp.citylist;

import java.util.ArrayList;
import java.util.List;

/**
 * ...
 * 
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public class SimpleCityList implements ListXYMutable
{
	private final List<HasXY> cities = new ArrayList<HasXY>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.epscor.uhhgatsp.HasXY#getX(int)
	 */
	@Override
	public double getX(int index) throws IndexOutOfBoundsException
	{
		return cities.get(index).getX();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.epscor.uhhgatsp.HasXY#getY(int)
	 */
	@Override
	public double getY(int index) throws IndexOutOfBoundsException
	{
		return cities.get(index).getY();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.epscor.uhhgatsp.HasXY#length()
	 */
	@Override
	public int length()
	{
		return cities.size();
	}

	@Override
	public void set(int index, double x, double y)
	{
		SimpleCity city = new SimpleCity(x, y);
		if (cities.size() <= index)
		{
			cities.add(city);
		}
		else
		{
			cities.set(index, city);
		}
	}

}
