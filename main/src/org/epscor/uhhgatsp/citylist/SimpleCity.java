package org.epscor.uhhgatsp.citylist;

/**
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public class SimpleCity implements HasXY
{

	private double[] position;

	SimpleCity(double x, double y)
	{
		position = new double[2];
		position[0] = x;
		position[1] = y;
	}

	@Override
	public double getX()
	{
		return position[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.epscor.uhhgatsp.HasXY#getY()
	 */
	@Override
	public double getY()
	{
		return position[1];
	}

}
