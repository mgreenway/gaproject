package org.epscor.uhhgatsp.citylist;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * 
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public class TSPReader
{

	private String start = "NODE_COORD_SECTION";
	private String end = "EOF";
	private String split = " ";

	private BufferedReader file;

	/**
	 * Opens the tsp file for reading. Must call close() when done reading.
	 * 
	 * @param fileName
	 *            The .tsp file to read cities from
	 */
	public void open(String fileName) throws FileNotFoundException
	{
		file = new BufferedReader(new FileReader(fileName));
	}

	/**
	 * Closes the file that was previously opened.
	 * 
	 * @throws IOException
	 */
	public void close() throws IOException
	{
		file.close();
	}

	/**
	 * @param newStart
	 *            The string to look for to tell that the following line will be
	 *            the start of the coordinates
	 */
	public void setStartString(String newStart)
	{
		start = newStart;
	}

	/**
	 * @param newEnd
	 *            What to look for as the end of the coordinate section
	 */
	public void setEndString(String newEnd)
	{
		end = newEnd;
	}

	/**
	 * @param newSplit
	 *            What to split the lines on
	 */
	public void setSplit(String newSplit)
	{
		split = newSplit;
	}

	/**
	 * Puts the cities from the file into list
	 * 
	 * @param list
	 *            The ListXYMutable to be populated with data from the file.
	 *            Must call open on a file before.
	 * @throws IOException
	 */
	public void readXY(ListXYMutable list) throws IOException
	{

		String line = "";
		int index = 0;

		while (!line.startsWith(start))
		{
			line = file.readLine();
		}

		line = file.readLine();

		while (!line.startsWith(end))
		{
			String[] coords = line.split(split);

			double x = Double.parseDouble(coords[1]);
			double y = Double.parseDouble(coords[2]);

			list.set(index, x, y);
			++index;

			line = file.readLine();
		}
	}

	/**
	 * Creates a new ListXY with the city data. This implementation creates a
	 * SimpleCityList
	 * 
	 * @return new SimpleCityList populated from file used in open
	 * @throws IOException
	 */
	public ListXY readXY() throws IOException
	{
		SimpleCityList list = new SimpleCityList();
		readXY(list);
		return list;
	}
}
