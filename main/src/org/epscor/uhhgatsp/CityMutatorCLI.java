package org.epscor.uhhgatsp;

import java.io.IOException;
import java.util.List;

import org.epscor.uhhgatsp.citylist.ListXY;
import org.epscor.uhhgatsp.citylist.TSPPrinter;
import org.epscor.uhhgatsp.citylist.TSPReader;
import org.epscor.uhhgatsp.mutator.CityListMutator;
import org.epscor.uhhgatsp.mutator.Mutator;
import org.epscor.uhhgatsp.shift.BoxMuller;
import org.epscor.uhhgatsp.shift.PointShift;

/**
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public class CityMutatorCLI
{

	private static String usage = "Requires command line arguments:"
			+ " \"filename standard-deviation [probability]\""
			+ "use --help for help";

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		handleArgs(args);

		ListXY oldCities;
		ListXY newCities;

		TSPReader reader = new TSPReader();
		TSPPrinter printer = new TSPPrinter();

		double stdDev;

		List<String> lines;

		Mutator mutator = new CityListMutator();
		PointShift shift = new BoxMuller();

		try
		{
			reader.open(args[0]);
			oldCities = reader.readXY();
		}
		catch (IOException e)
		{
			System.out.println("The file given was not a valid tsp file.");
			return;
		}

		try
		{
			stdDev = Double.parseDouble(args[1]);
		}
		catch (NumberFormatException e)
		{
			System.out
					.println("The standard deviation was not a floating point number.");
			return;
		}

		if (args.length < 3)
		{
			newCities = mutator.Mutate(oldCities, shift, stdDev);
		}
		else
		{
			double mutProb;
			try
			{
				mutProb = Double.parseDouble(args[2]);
			}
			catch (NumberFormatException e)
			{
				System.out
						.println("The mutation probability was not a floating point number.");
				return;
			}
			newCities = mutator.Mutate(oldCities, shift, stdDev, mutProb);
		}

		lines = printer.print(newCities);

		for (String line : lines)
		{
			System.out.println(line);
		}
	}

	public static void handleArgs(String[] args)
	{
		if (args.length == 0)
		{
			System.out.println(usage);
			System.exit(1);
		}
		if (args[0].equals("help") || args[0].equals("-help")
				|| args[0].equals("--help"))
		{
			System.out.println("");
			System.exit(1);
		}
		if (args.length < 2)
		{
			System.out.println(usage);
			System.exit(1);
		}
	}
}
