package org.epscor.uhhgatsp.shift;

/**
 * For each city list we use the same mutator because there may be sequential
 * logic built into how some of the functions work such as Box-Muller
 * 
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public interface PointShift
{
	double newValue(double mean, double standardDeviation);
}
