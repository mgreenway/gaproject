package org.epscor.uhhgatsp.mutator;

import static org.junit.Assert.*;

import org.epscor.uhhgatsp.citylist.ListXY;
import org.epscor.uhhgatsp.citylist.SimpleCityList;
import org.epscor.uhhgatsp.shift.BoxMuller;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public class CityListMutatorShould
{

	SimpleCityList oldCities = new SimpleCityList();

	@Before
	public void init()
	{
		oldCities = new SimpleCityList();

		for (int i = 0; i < 10; i++)
		{
			oldCities.set(i, 42.1138, 1.0);
		}
	}

	@Test
	public void mutateNoCitiesOnZero()
	{
		CityListMutator mutator = new CityListMutator();

		ListXY newCities = mutator.Mutate(oldCities, new BoxMuller(), 0);

		for (int i = 0; i < 10; i++)
		{
			assertEquals(oldCities.getX(i), newCities.getX(i), 0.0);
			assertEquals(oldCities.getY(i), newCities.getY(i), 0.0);
		}
	}

	/**
	 * Once again just because this fails does not mean it is broken just double
	 * check things. Almost 100% of the time the mutations should fall within
	 * 3*stdDev.
	 */
	@Test
	public void mutateCitiesNormal()
	{
		CityListMutator mutator = new CityListMutator();

		double stdDev = 4.2;

		ListXY newCities = mutator.Mutate(oldCities, new BoxMuller(), stdDev);

		for (int i = 0; i < 10; i++)
		{
			assertEquals(oldCities.getX(i), newCities.getX(i), 3.0 * stdDev);
			assertEquals(oldCities.getY(i), newCities.getY(i), 3.0 * stdDev);
		}
	}
}
