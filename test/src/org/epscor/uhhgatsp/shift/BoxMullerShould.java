package org.epscor.uhhgatsp.shift;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public class BoxMullerShould
{

	/**
	 * When the standard deviation is 0 we should get the same value returned as
	 * the one that is input
	 */
	@SuppressWarnings("static-method")
	@Test
	public void duplicateOnZero()
	{
		BoxMuller boxMuller = new BoxMuller();
		assertEquals(boxMuller.newValue(5.26, 0.0), 5.26, 0.0);
	}

	/**
	 * If we are following a normal distribution we should "always" just because
	 * the assertions fail sometimes doesn't mean it is broken, just double
	 * check fall within 3*stdDev.
	 */
	@SuppressWarnings("static-method")
	@Test
	public void followNormal()
	{
		BoxMuller boxMuller = new BoxMuller();
		double stdDev = 1.0;

		assertEquals(boxMuller.newValue(5.26, stdDev), 5.26, 3.0 * stdDev);
		assertEquals(boxMuller.newValue(987, stdDev), 987, 3.0 * stdDev);

		stdDev = 5.0;

		assertEquals(boxMuller.newValue(5.26, stdDev), 5.26, 3.0 * stdDev);
		assertEquals(boxMuller.newValue(987, stdDev), 987, 3.0 * stdDev);
	}

}
