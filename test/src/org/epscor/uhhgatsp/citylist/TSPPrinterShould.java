package org.epscor.uhhgatsp.citylist;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public class TSPPrinterShould
{
	TSPPrinter printer;
	List<String> lines;

	@Before
	public void init()
	{
		printer = new TSPPrinter();
		TSPReader reader = new TSPReader();
		try
		{
			reader.open("test\\ch130.tsp");
			lines = printer.print(reader.readXY());
		}
		catch (Exception e)
		{
			assertTrue(false);
		}
	}

	@Test
	public void haveCorrectDimension()
	{
		assertEquals(lines.get(3), "DIMENSION: 130");
	}

	@Test
	public void haveCorrectLines()
	{
		assertEquals(lines.get(135), "130 403.2874386776 205.8971749407");
	}
}
