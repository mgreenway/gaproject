package org.epscor.uhhgatsp.citylist;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public class SimpleCityListShould
{

	@SuppressWarnings("static-method")
	@Test
	public void haveCorrectLength()
	{
		SimpleCityList cities = new SimpleCityList();
		assertEquals(cities.length(), 0);

		cities.set(0, 1.2, 1.2);
		assertEquals(cities.length(), 1);

		cities.set(1, 1.2, 1.2);
		assertEquals(cities.length(), 2);

		cities.set(2, 1.2, 1.2);
		assertEquals(cities.length(), 3);
	}

	@SuppressWarnings("static-method")
	@Test
	public void haveCorrectValues()
	{
		SimpleCityList cities = new SimpleCityList();
		cities.set(0, 1.23, 3.21);
		assertEquals(cities.getX(0), 1.23, 0.00001);
		assertEquals(cities.getY(0), 3.21, 0.00001);

		cities.set(1, 99.45, -42.66);
		assertEquals(cities.getX(1), 99.45, 0.00001);
		assertEquals(cities.getY(1), -42.66, 0.00001);
	}
}
