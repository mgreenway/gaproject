package org.epscor.uhhgatsp.citylist;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Matthew Greenway <mgreenway@uchicago.edu>
 * 
 */
public class TSPReaderShould
{
	TSPReader reader;

	@Before
	public void init()
	{
		reader = new TSPReader();
		try
		{
			reader.open("test\\ch130.tsp");
		}
		catch (FileNotFoundException e)
		{
			assertTrue(false);
		}
	}

	@After
	public void done()
	{
		try
		{
			reader.close();
		}
		catch (IOException e)
		{
			assertTrue(false);
		}
	}

	@Test
	public void readCorrectValues()
	{
		try
		{
			ListXY list = reader.readXY();
			assertEquals(list.getX(0), 334.5909245845, 0.0);
		}
		catch (IOException e)
		{
			assertTrue(false);
		}

	}

	@Test
	public void readAllValues()
	{
		try
		{
			ListXY list = reader.readXY();
			assertEquals(list.length(), 130);
			assertEquals(list.getY(129), 205.8971749407, 0.0);
		}
		catch (IOException e)
		{
			assertTrue(false);
		}
	}
}
